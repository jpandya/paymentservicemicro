package com.jiten.innovations.springCloud.PaymentServiceMicro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentServiceMicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentServiceMicroApplication.class, args);
	}

}
